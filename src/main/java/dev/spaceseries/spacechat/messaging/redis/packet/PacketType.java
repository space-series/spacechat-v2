package dev.spaceseries.spacechat.messaging.redis.packet;

public enum PacketType {
    CHAT,
    BROADCAST
}
