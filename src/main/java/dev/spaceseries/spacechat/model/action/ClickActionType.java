package dev.spaceseries.spacechat.model.action;

public enum  ClickActionType {
    RUN_COMMAND,
    OPEN_URL,
    SUGGEST_COMMAND
}
